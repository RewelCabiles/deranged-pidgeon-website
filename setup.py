from setuptools import setup

setup(
    name="RewelsWebCV",
    packages=[],
    version="0.1",
    include_package_data=True,
    description="A Web Based Versions Of My 'Accomplishments'",
    zip_safe=False,
    install_requires=[
        'flask',
        'Flask-WTF',
        'WTforms',
        'flask-login',
        'flask-bcrypt',
        'flask_pymongo',
        'dnspython',
        'SQLAlchemy'
        ]
    )
