from flask import Flask, render_template
from flask_sqlalchemy import SQLAlchemy

app = Flask(__name__)
app.config.from_object('config')

db = SQLAlchemy(app)

# Import a module / component using its blueprint handler variable (mod_auth)
from app.App.routes import App as app_module
app.register_blueprint(app_module)
db.create_all()