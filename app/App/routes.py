# Import flask dependencies
from flask import render_template, Blueprint, request, session, flash, redirect, url_for
import flask_bcrypt

from app import db
from app.App.forms import LoginForm, PostForm
from app.App.models import User, Post

# Define the blueprint: 'auth', set its url prefix: app.url/auth
App = Blueprint('App', __name__, url_prefix='/')
# Routes

@App.route("/logout")
def sign_out():
    session.pop('username', None)
    return redirect(url_for('App.index'))

@App.route("/login", methods=['GET', 'POST'])
def sign_in():
    form = LoginForm(request.form)
    if form.validate_on_submit():
        user = User.query.filter_by(username=form.username.data).first()
        if user and flask_bcrypt.check_password_hash(user.password, form.password.data):
            session['username'] = user.username
            return redirect(url_for('App.index'))
        flash('Wrong email or password', 'error-message')

    return render_template('App/login.html', form=form)

@App.route("/")
def index():
    posts = Post.query.order_by(Post.id.desc()).all()
    return render_template('App/index.html', posts=posts)

@App.route("/new_post", methods=['GET', 'POST'])
def new_post():
    if "username" not in session:
        return redirect(url_for('App.index'))
    form = PostForm(request.form)
    if form.validate_on_submit():
        user = User.query.filter_by(username=session["username"]).first()
        new_post = Post(title=form.title.data, body=form.body.data, author=user)
        user.posts.append(new_post)
        db.session.add(user)
        db.session.commit()
        return redirect(url_for('App.index'))



    return render_template('App/new_post.html', form=form)



if __name__ == '__main__':
    app.run(debug=True)
