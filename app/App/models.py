from flask_sqlalchemy import SQLAlchemy
from flask_bcrypt import Bcrypt
from app import db

# Helpers
def create_account(username, password):
	hashed_password = bcrypt.generate_password_hash(password)
	new_account = User(username=username, password=hashed_password)
	db.session.add(new_account)
	db.session.commit()

# Models
class User(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(80), unique=True, nullable=False)
    password = db.Column(db.String(80), unique=False, nullable=False)
    def __repr__(self):
        return '<User %r>' % self.username

class Post(db.Model):
	id = db.Column(db.Integer, primary_key=True)
	title = db.Column(db.String(80), nullable=False)
	body = db.Column(db.Text, nullable=False)
	author = db.relationship('User', backref=db.backref('posts', lazy=True))
	author_id = db.Column(db.Integer, db.ForeignKey('user.id'),nullable=False)


