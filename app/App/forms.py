from flask_wtf import Form
from wtforms import TextField, PasswordField, TextAreaField
from wtforms.validators import Required, Email, EqualTo


# Define the login form (WTForms)

class LoginForm(Form):
    username    = TextField('Username', [Required(message='You need to enter a username duh')])
    password = PasswordField('Password', [Required(message='Ey whats the password')])


class PostForm(Form):
	title = TextField('Title', [Required(message="Ey you need a title")])
	body = TextAreaField('Body', [Required(message="Ey you need a title")], render_kw={"rows": 70, "cols": 70})